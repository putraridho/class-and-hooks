import React from 'react';
import ClassComponent from './pages/ClassComponent'
import FunctionalComponent from './pages/FuncitonalComponent';

function App() {
  return (
    <div className="App">
      <ClassComponent text="This is from props" />
      <hr />
      <FunctionalComponent text="This is from props" />
    </div>
  );
}

export default App;

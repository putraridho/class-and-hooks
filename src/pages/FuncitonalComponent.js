import React, { useState } from "react";

const FunctionalComponent = ({ text }) => {
  const [counter, setCounter] = useState(0);
  const [form, setForm] = useState({ firstName: "John", lastName: "Doe" });

  const handleCount = type => {
    switch (type) {
      case "up":
        return setCounter(counter + 1);
      default:
        return setCounter(counter - 1);
    }
  };

  const handleText = e => setForm({ ...form, [e.target.name]: e.target.value });

  return (
    <>
      <h1>Functional Component</h1>
      <h3>Prop Text: {text}</h3>
      <div>
        <p>
          Counter: <b>{counter}</b>
        </p>
        <button onClick={() => handleCount("up")}>Count Up</button>
        <button onClick={() => handleCount("down")}>Count Down</button>
      </div>
      <div>
        <p>
          name:{" "}
          <b>
            {form.firstName} {form.lastName}
          </b>
        </p>
        <input
          type="text"
          name="firstName"
          value={form.firstName}
          onChange={e => handleText(e)}
        />
        <input
          type="text"
          name="lastName"
          value={form.lastName}
          onChange={e => handleText(e)}
        />
      </div>
    </>
  );
};

export default FunctionalComponent;

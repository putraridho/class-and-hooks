import React from "react";

class ClassComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      counter: 0,
      firstName: "John",
      lastName: "Doe"
    };
  }

  handleCount(type) {
    switch (type) {
      case "up":
        return this.setState({ counter: this.state.counter + 1 });
      default:
        return this.setState({ counter: this.state.counter - 1 });
    }
  }

  handleText(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  render() {
    return (
      <>
        <h1>Class Component</h1>
        <h3>Prop Text: {this.props.text}</h3>
        <div>
          <p>
            Counter: <b>{this.state.counter}</b>
          </p>
          <button onClick={() => this.handleCount("up")}>Count Up</button>
          <button onClick={() => this.handleCount("down")}>Count Down</button>
        </div>
        <div>
          <p>
            name:{" "}
            <b>
              {this.state.firstName} {this.state.lastName}
            </b>
          </p>
          <input
            type="text"
            name="firstName"
            value={this.state.firstName}
            onChange={e => this.handleText(e)}
          />
          <input
            type="text"
            name="lastName"
            value={this.state.lastName}
            onChange={e => this.handleText(e)}
          />
        </div>
      </>
    );
  }
}

export default ClassComponent;
